//
//  TimezAppDelegate.h
//  Timez
//
//  Created by Malin Pihl on 2012-03-17.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "TimezViewController.h"
#import "StartViewController.h"
#import "GCDAsyncSocket.h"

#define ZETA_IP "192.168.1.1"
#define ZETA_PORT 4711
#import "RegionsViewController.h"

#define UIAppDelegate \
((TimezAppDelegate *)[UIApplication sharedApplication].delegate)

@interface TimezAppDelegate : NSObject <UIApplicationDelegate, GCDAsyncSocketDelegate> {

    UINavigationController *navController;
    StartViewController *startView;
    GCDAsyncSocket *zeta;
    RegionsViewController *region;
}



@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) TimezViewController *timezView;
@property (nonatomic, retain) RegionsViewController *region;
@property (nonatomic, retain) UINavigationController *navController;
//@property (nonatomic, retain) GCDAsyncSocket *zeta;

- (void)goToPage: (id)sender;
- (void)socket:(GCDAsyncSocket *)sender didReadData:(NSData *)data withTag:(long)tag;
- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag;

@end


