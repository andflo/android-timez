//
//  CustomerData.m
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomerData.h"

@implementation CustomerData
@synthesize customerTitle;
@synthesize defaultCustomer;  //YES this is default customer
@synthesize active; //NO = old customer not currently used
@synthesize tariff;
@synthesize percentRam;
@synthesize timeTot;  //total acumulated time this customer
@synthesize timeMonth;  //time reported this month
@end
