//
//  CustomerData.h
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerData : NSObject

@property (assign) NSString *customerTitle;
@property (assign) BOOL *defaultCustomer;  //YES this is default customer
@property (assign) BOOL *active; //NO = old customer not currently used
@property (assign) NSInteger tariff;
@property (assign) NSInteger percentRam;
@property (assign) NSInteger timeTot;  //total acumulated time this customer
@property (assign) NSInteger timeMonth;  //time reported this month

@end
