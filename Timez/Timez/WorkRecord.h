//
//  WorkRecord.h
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerData.h"

@interface WorkRecord : NSObject

@property (assign) float *lunch; //lunch in hours. Should probably only alow 0.5 interval
@property (assign) BOOL *lunchIncluded; //YES = Lunch is in start->end time and should be substracted
@property (assign) CustomerData *dayDate;
@property (assign) NSDate *startTime;
@property (assign) NSDate *endTime;
@end
